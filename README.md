# About the app #
# JamoSolutionsTest #
This application demonstrates the two egg 100 storey building.
It Takes a single input which is the number of floors from which an egg is dropped. If the egg does not break from the floor entered by the user, the application automatically moves further to look for the floor from which the egg would not have broken at all.

# Setup #
Android Studio version 1.2 was used during development but the latest versions of android studio can be used
- Install Android Studio
- Import this project from Git via VCS
- Android build version 23.0.1 was thus if prompted to download and install the same please go ahead!
- A testable APK has been added to the main folder of the app is you wish to have a look at it too

# Testing #
This application has been tested on an android device running Lolipop

# Where to locate the build #
The APK file would be located in the folder /app/build/outputs/apk/

# Whom to contact #
Incase of any trouble or enhancements please contact: sdhara2@hotmail.com
