package com.jamosolutions.dharashah;

import android.app.Application;
import android.content.Context;

import com.jamosolutions.dharashah.network.NetworkUtilities;

/**
 * Created by Dhara Shah on 20-01-2016.
 */
public class JamoSolutionsApp extends Application {
    private static JamoSolutionsApp mApp;
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        mApp = this;
        initNetwork();
    }

    /**
     * Gets the application instance - a singleton
     * @return
     */
    public static JamoSolutionsApp getAppContext(){
        if(mApp == null){
            mApp= (JamoSolutionsApp)mContext;
        }
        return mApp;
    }

    /**
     * Initializes network related resources
     */
    private void initNetwork(){
        NetworkUtilities.init();
    }
}
