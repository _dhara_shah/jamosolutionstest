package com.jamosolutions.dharashah.manager;

import com.jamosolutions.dharashah.model.EggResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dhara Shah on 24-01-2016. </br>
 * Stores the list of items to be displayed to the ueer (trials)
 */
public class SessionManager {
    private static SessionManager manager;
    private List<EggResult> mEggResultList;

    /**
     * Constructor
     */
    public SessionManager(){
        mEggResultList = new ArrayList<>();
    }

    /**
     * Gets the singleton instance
     * @return
     */
    public static SessionManager getInstance(){
        if(manager == null){
            manager = new SessionManager();
        }
        return manager;
    }

    /**
     * Gets the list of trials for the user
     * @return
     */
    public List<EggResult> getEggResultList() {
        return mEggResultList;
    }

    /**
     * Sets the list of trials
     * @param fallResultList
     */
    public void setEggResultList(List<EggResult> fallResultList) {
        this.mEggResultList = fallResultList;
    }
}
