package com.jamosolutions.dharashah.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.appspot.twoeggs100storeybuilding.tallbuilding.model.FallResult;
import com.jamosolutions.dharashah.R;
import com.jamosolutions.dharashah.model.EggResult;

import java.util.List;

/**
 * Created by USER on 24-01-2016.
 */
public class MyAdapter extends ArrayAdapter<EggResult> {
    private Context mContext;
    private int RESOURCE;
    private List<EggResult> mEggResults;

    /**
     * Constructor
     * @param context
     * @param resource
     * @param objects
     */
    public MyAdapter(Context context, int resource, List<EggResult> objects) {
        super(context, resource, objects);
        mContext = context;
        RESOURCE = resource;
        mEggResults = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder vh = null;
        if(view == null) {
            view = LayoutInflater.from(mContext).inflate(RESOURCE, parent, false);
            vh = new ViewHolder();
            vh.txtEggId = (TextView)view.findViewById(R.id.txtEggId);
            vh.txtFloorNumber = (TextView)view.findViewById(R.id.txtFloorNumb);
            vh.txtHasBroken = (TextView)view.findViewById(R.id.txtHasBroken);
            view.setTag(vh);
        }else {
            vh = (ViewHolder)view.getTag();
        }

        // displays the result in the listview
        EggResult eggResult = mEggResults.get(position);
        vh.txtEggId.setText(""+eggResult.getFallResult().getId());
        vh.txtFloorNumber.setText(""+eggResult.getFloorNumber());
        vh.txtHasBroken.setText((eggResult.getFallResult().getBroken()) ? "YES" : "NO");

        return view;
    }

    class ViewHolder {
        TextView txtEggId;
        TextView txtHasBroken;
        TextView txtFloorNumber;
    }
}
