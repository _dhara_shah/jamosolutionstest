package com.jamosolutions.dharashah.asynctasks;

import android.os.AsyncTask;

import com.appspot.twoeggs100storeybuilding.tallbuilding.model.FallResult;
import com.jamosolutions.dharashah.callback.IResponseListener;
import com.jamosolutions.dharashah.model.CheckResult;
import com.jamosolutions.dharashah.network.NetworkUtilities;

import java.io.IOException;

/**
 * Created by Dhara Shah on 24-01-2016.
 */
public class CheckEggsAsyncTask extends AsyncTask<Void,Void,CheckResult> {
    private long mEggId;
    private int mFloorNumber;
    private IResponseListener mListener;

    public CheckEggsAsyncTask(long eggId, int floorNumber, IResponseListener listener) {
        mEggId = eggId;
        mFloorNumber = floorNumber;
        mListener = listener;
    }

    @Override
    protected CheckResult doInBackground(Void... voids) {
        try {
            FallResult fallResult = NetworkUtilities.getInstance()
                    .getTallBuildingInstance().tallBuilding().checkEgg(mEggId, mFloorNumber).execute();

            CheckResult checkResult = new CheckResult();
            checkResult.setFallResult(fallResult);
            checkResult.setFloorNumber(mFloorNumber);

            return checkResult;
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(CheckResult result) {
        super.onPostExecute(result);
        if(mListener != null) mListener.onResponseReceived(result);
    }
}
