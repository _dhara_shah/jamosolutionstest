package com.jamosolutions.dharashah.asynctasks;

import android.os.AsyncTask;

import com.appspot.twoeggs100storeybuilding.tallbuilding.model.EggsId;
import com.jamosolutions.dharashah.callback.IResponseListener;
import com.jamosolutions.dharashah.network.NetworkUtilities;

import java.io.IOException;

/**
 * Created by Dhara Shah on 24-01-2016.
 */
public class TakeEggsAsyncTask extends AsyncTask<Void, Void, EggsId> {
    private IResponseListener mListener;
    public TakeEggsAsyncTask(IResponseListener listener) {
        mListener = listener;
    }

    @Override
    protected EggsId doInBackground(Void... voids) {
        try {
            EggsId eggsId = NetworkUtilities.getInstance()
                    .getTallBuildingInstance().tallBuilding().takeEggs().execute();
            return eggsId;
        }catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(EggsId eggsId) {
        super.onPostExecute(eggsId);
        if(mListener != null) mListener.onResponseReceived(eggsId);
    }
}
