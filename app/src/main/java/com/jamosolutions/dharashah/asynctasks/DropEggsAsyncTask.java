package com.jamosolutions.dharashah.asynctasks;

import android.os.AsyncTask;

import com.appspot.twoeggs100storeybuilding.tallbuilding.model.FallResult;
import com.jamosolutions.dharashah.callback.IResponseListener;
import com.jamosolutions.dharashah.model.EggResult;
import com.jamosolutions.dharashah.network.NetworkUtilities;

import java.io.IOException;
import java.util.Stack;

/**
 * Created by Dhara Shah on 24-01-2016. </br>
 * Drops the eggs obtained
 */
public class DropEggsAsyncTask extends AsyncTask<Void,Void,EggResult> {
    private long mEggId;
    private int mFloorNumber;
    private IResponseListener mListener;
    private EggResult mEggResult;
    private boolean mIsRecursive;
    private boolean mTraverseFlag;
    private Stack<EggResult> mStackResult;

    public DropEggsAsyncTask(long eggId,int floorNumber,boolean isRecursive,
                             boolean traverseFlag, IResponseListener listener) {
        mEggId = eggId;
        mFloorNumber = floorNumber;
        mListener = listener;
        mIsRecursive = isRecursive;
        mTraverseFlag = traverseFlag;
        mStackResult = new Stack<>();
    }

    @Override
    protected EggResult doInBackground(Void... voids) {
        try {
            if(!mIsRecursive) {
                FallResult fallResult = NetworkUtilities.getInstance()
                        .getTallBuildingInstance().tallBuilding().dropEgg(mEggId, mFloorNumber).execute();

                mEggResult = new EggResult();
                mEggResult.setFloorNumber(mFloorNumber);
                mEggResult.setFallResult(fallResult);

                return mEggResult;
            }else {
                // continue looking for another floor where the egg might not break
                // if the egg does not break at nth floor it means it will not break below that
                // we therefore look for floors above n where it might break
                // For eg: if floor = 13, look for floors above 13 that is 14 etc until it might break
                while(true) {
                    if(mTraverseFlag) {
                        mFloorNumber = mFloorNumber - 1;
                    }else {
                        mFloorNumber = mFloorNumber + 1;
                    }

                    // floor numbers should be between 1 and 100
                    if(mFloorNumber >= 1 && mFloorNumber <= 100) {
                        FallResult fallResult = NetworkUtilities.getInstance()
                                .getTallBuildingInstance().tallBuilding().dropEgg(mEggId, mFloorNumber).execute();

                        EggResult eggResult = new EggResult();
                        eggResult.setFallResult(fallResult);
                        eggResult.setFloorNumber(mFloorNumber);

                        mStackResult.push(eggResult);
                        // add this fall to the list and notify data set changes
                        publishProgress(voids);

                        // break this while loop
                        // we have got the floor number
                        if((fallResult.getBroken() && !mTraverseFlag) ||
                                (!fallResult.getBroken() && mTraverseFlag)) {
                            break;
                        }
                    }else {
                        // break this loop here
                        break;
                    }
                }
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        if(mListener != null) {
            mListener.onFloorChanged(mFloorNumber);

            while (!mStackResult.isEmpty()) {
                mListener.onResponseReceived(mStackResult.pop());
            }
        }
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(EggResult eggResult) {
        super.onPostExecute(eggResult);
        if(!mIsRecursive) {
            if(mListener != null) mListener.onResponseReceived(eggResult);
        }else {
            if(mListener != null) mListener.onResponseReceived(mTraverseFlag ? mFloorNumber : mFloorNumber-1);
        }
    }
}
