package com.jamosolutions.dharashah.utilities;

import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

import com.jamosolutions.dharashah.JamoSolutionsApp;
import com.jamosolutions.dharashah.R;
import com.rengwuxian.materialedittext.MaterialEditText;

/**
 * Created by Dhara Shah on 24-01-2016.
 */
public class CustomValidator implements TextWatcher{
    private MaterialEditText mEditText;

    public CustomValidator(MaterialEditText editText) {
        mEditText = editText;
    }

    /**
     * Check error function for all the edittext used in application.
     *
     * @param editText
     * @return boolean
     */
    public static boolean checkError(MaterialEditText editText) {
        switch (editText.getId()) {
            case R.id.edtFloor:
                if (TextUtils.isEmpty(editText.getText().toString())) {
                    editText.setError("Please enter the floor number");
                    editText.setUnderlineColor(ContextCompat.getColor(JamoSolutionsApp.getAppContext(), R.color.error_color));
                    editText.setHintTextColor(ContextCompat.getColor(JamoSolutionsApp.getAppContext(), R.color.error_color));
                    editText.requestFocus();
                    return true;
                }else {
                    boolean isError = false;
                    try {
                        if(Integer.parseInt(editText.getText().toString().trim()) <= 0 ||
                                Integer.parseInt(editText.getText().toString().trim()) > 100) {
                            isError = true;
                        }
                    }catch (NumberFormatException e) {
                        e.printStackTrace();
                        isError = true;
                    }

                    if(isError) {
                        editText.setError("Please enter floor number between 1 and 100");
                        editText.setUnderlineColor(ContextCompat.getColor(JamoSolutionsApp.getAppContext(), R.color.error_color));
                        editText.setHintTextColor(ContextCompat.getColor(JamoSolutionsApp.getAppContext(), R.color.error_color));
                        editText.requestFocus();
                    }
                }
                break;
        }
        return false;

    }

    /*
     * (non-Javadoc)
     *
     * @see android.text.TextWatcher#beforeTextChanged(java.lang.CharSequence, int, int, int)
     */
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    /*
     * (non-Javadoc)
     *
     * @see android.text.TextWatcher#onTextChanged(java.lang.CharSequence, int, int, int)
     */
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (!checkError(mEditText)) {
            mEditText.setUnderlineColor(ContextCompat.getColor(JamoSolutionsApp.getAppContext(), R.color.colorPrimary));
            mEditText.setHintTextColor(ContextCompat.getColor(JamoSolutionsApp.getAppContext(), R.color.hint_color));
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see android.text.TextWatcher#afterTextChanged(android.text.Editable)
     */
    @Override
    public void afterTextChanged(Editable s) {
    }
}
