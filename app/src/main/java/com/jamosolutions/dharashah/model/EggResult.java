package com.jamosolutions.dharashah.model;

import com.appspot.twoeggs100storeybuilding.tallbuilding.model.FallResult;

/**
 * Created by Dhara Shah on 24-01-2016.
 */
public class EggResult {
    private FallResult fallResult;
    private int floorNumber;

    public FallResult getFallResult() {
        return fallResult;
    }

    public void setFallResult(FallResult fallResult) {
        this.fallResult = fallResult;
    }

    public int getFloorNumber() {
        return floorNumber;
    }

    public void setFloorNumber(int floorNumber) {
        this.floorNumber = floorNumber;
    }
}
