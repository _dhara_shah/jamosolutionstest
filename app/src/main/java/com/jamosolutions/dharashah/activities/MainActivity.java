package com.jamosolutions.dharashah.activities;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.appspot.twoeggs100storeybuilding.tallbuilding.model.EggsId;
import com.appspot.twoeggs100storeybuilding.tallbuilding.model.FallResult;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.jamosolutions.dharashah.JamoSolutionsApp;
import com.jamosolutions.dharashah.R;
import com.jamosolutions.dharashah.adapters.MyAdapter;
import com.jamosolutions.dharashah.callback.IResponseListener;
import com.jamosolutions.dharashah.manager.SessionManager;
import com.jamosolutions.dharashah.model.CheckResult;
import com.jamosolutions.dharashah.model.EggResult;
import com.jamosolutions.dharashah.network.RestClient;
import com.jamosolutions.dharashah.utilities.CustomExceptionHandler;
import com.jamosolutions.dharashah.utilities.CustomValidator;
import com.jamosolutions.dharashah.network.NetworkUtilities;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.List;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, IResponseListener {
    private MaterialEditText mEdtFloor;
    private FloatingActionMenu mFabMenu;
    private FloatingActionButton mFabStart;
    private FloatingActionButton mFabClear;
    private ListView mLstView;
    private EggsId mEggsId;
    private int mFloorNumber;
    private MyAdapter mAdapter;
    private IResponseListener mListener;
    private ProgressBar mProgressBar;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initCrashThread();
        initToolbar();
        initView();
    }

    /**
     * Initializes logging mechanism when there is a crash
     */
    private void initCrashThread() {
        if(!(Thread.getDefaultUncaughtExceptionHandler() instanceof CustomExceptionHandler)) {
            Thread.setDefaultUncaughtExceptionHandler(new CustomExceptionHandler(this));
        }
    }

    /**
     * Initializes the toolbar
     */
    private void initToolbar() {
        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }

    /**
     * Initializes the views
     */
    private void initView(){
        mListener = this;
        mLstView = (ListView)findViewById(android.R.id.list);
        mFabMenu = (FloatingActionMenu)findViewById(R.id.menu);
        mFabStart = (FloatingActionButton)findViewById(R.id.fabStart);
        mFabClear = (FloatingActionButton)findViewById(R.id.fabClear);
        mEdtFloor = (MaterialEditText)findViewById(R.id.edtFloor);
        mProgressBar = (ProgressBar)findViewById(R.id.progressBar);

        mFabStart.setOnClickListener(this);
        mFabClear.setOnClickListener(this);

        // sets the colors to the floating buttons and also floating action buttons
        mFabMenu.setMenuButtonColorNormal(ContextCompat.getColor(JamoSolutionsApp.getAppContext(),
                R.color.colorPrimary));
        mFabMenu.setMenuButtonColorPressed(ContextCompat.getColor(JamoSolutionsApp.getAppContext(),
                R.color.colorPrimaryDark));
        mFabStart.setColorNormal(ContextCompat.getColor(JamoSolutionsApp.getAppContext(),
                R.color.colorPrimary));
        mFabStart.setColorPressed(ContextCompat.getColor(JamoSolutionsApp.getAppContext(),
                R.color.colorPrimaryDark));
        mFabClear.setColorNormal(ContextCompat.getColor(JamoSolutionsApp.getAppContext(),
                R.color.colorPrimary));
        mFabClear.setColorPressed(ContextCompat.getColor(JamoSolutionsApp.getAppContext(),
                R.color.colorPrimaryDark));

        // set the adapter to an empty list initially
        // would be notified later on
        mAdapter = new MyAdapter(JamoSolutionsApp.getAppContext(),
                R.layout.individual_row, SessionManager.getInstance().getEggResultList());
        mLstView.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabStart:
                // start calculations here
                validateStart();
                mFabMenu.close(true);
                break;

            case R.id.fabClear:
                // clear the list and refresh the listview
                mFabMenu.close(true);
                SessionManager.getInstance().getEggResultList().clear();
                notifyAdapter();
                break;
        }
    }

    /**
     * Validates data before initiating service calls
     */
    private void validateStart(){
        if(CustomValidator.checkError(mEdtFloor))
            return;

        // start finding the highest floor
        int floorNumber = Integer.parseInt(mEdtFloor.getText().toString().trim());
        mFloorNumber = floorNumber;

        if(RestClient.isNetworkAvailable()) {
            mProgressBar.setVisibility(View.VISIBLE);
            // take the eggs here
            NetworkUtilities.getInstance().takeEggs(mListener);
        }else {
            Toast.makeText(JamoSolutionsApp.getAppContext(),
                    "Please check your internet connection",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResponseReceived(Object object) {
        mProgressBar.setVisibility(View.GONE);
        if(object != null) {
            if(object instanceof EggsId) {
                mEggsId = (EggsId)object;

                if(RestClient.isNetworkAvailable()) {
                    // drop the eggs that we have received
                    NetworkUtilities.getInstance().dropEggs(mEggsId.getId(),
                            mFloorNumber,false, false, mListener);
                }else {
                    Toast.makeText(JamoSolutionsApp.getAppContext(),
                            "Please check your internet connection",
                            Toast.LENGTH_LONG).show();
                }
            }else if(object instanceof EggResult) {
                EggResult eggResult = (EggResult)object;

                // stores the result into the sessionmanger to notify the data set
                List<EggResult> resultList = SessionManager.getInstance().getEggResultList();
                resultList.add(eggResult);
                SessionManager.getInstance().setEggResultList(resultList);

                // notifies the listview on the main thread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        notifyAdapter();
                    }
                });

                // only if the result is obtained the first time
                // do we make a second webservice call
                // this methos is called repeatatively from the publishProgress
                if(resultList.size() <= 1) {
                    findHighestFloor(eggResult.getFallResult());
                }

            }else if(object instanceof Integer) {
                Log.e("dhara", "final floor number >>> " + (Integer) object);

                if(RestClient.isNetworkAvailable()) {
                    mProgressBar.setVisibility(View.VISIBLE);

                    // make a call to verify the result
                    NetworkUtilities.getInstance().checkEgg(mEggsId.getId(),
                            (Integer)object, mListener);
                }
            }else if(object instanceof CheckResult) {
                CheckResult result = ((CheckResult) object);
                int floorNumber = result.getFloorNumber();
                Toast.makeText(JamoSolutionsApp.getAppContext(),
                        "Highest floor. Result verified : " + floorNumber +
                                "\nNumber of tries : " + result.getFallResult().getTries(),
                        Toast.LENGTH_LONG).show();
            }
        }else {
            // connection issues
            // check internet connection
            Toast.makeText(JamoSolutionsApp.getAppContext(),
                    "Server communication error. Please try again!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFloorChanged(int floorNumber) {
        mFloorNumber = floorNumber;
    }

    /**
     * Notifies the adapter
     */
    private void notifyAdapter(){
        if(mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Makes a call to get the highest floor from which the egg will not break </br>
     * It is a recursive function
     * @param result
     */
    private void findHighestFloor(FallResult result){
        boolean flag = result.getBroken();
        if(RestClient.isNetworkAvailable()) {
            NetworkUtilities.getInstance().dropEggs(mEggsId.getId(), mFloorNumber, true, flag, mListener);
        }else {
            Toast.makeText(JamoSolutionsApp.getAppContext(),
                    "Please check your internet connection", Toast.LENGTH_LONG).show();
        }
    }
}
