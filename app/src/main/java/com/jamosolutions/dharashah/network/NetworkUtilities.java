package com.jamosolutions.dharashah.network;

import com.appspot.twoeggs100storeybuilding.tallbuilding.Tallbuilding;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.jamosolutions.dharashah.asynctasks.CheckEggsAsyncTask;
import com.jamosolutions.dharashah.asynctasks.DropEggsAsyncTask;
import com.jamosolutions.dharashah.asynctasks.TakeEggsAsyncTask;
import com.jamosolutions.dharashah.callback.IResponseListener;

/**
 * Created by Dhara Shah on 24-01-2016.
 */
public class NetworkUtilities {
    private static Tallbuilding mTallBuilding;
    private static NetworkUtilities mNetworkUtilities;

    /**
     * Initializes the resources needed
     */
    public static void init() {
        mNetworkUtilities = new NetworkUtilities();
        initTallBuildingInstance();
    }

    /**
     * Initializes the building class which would be used to get data
     */
    private static void initTallBuildingInstance(){
        if(mTallBuilding == null) {
            mTallBuilding = new Tallbuilding(AndroidHttp.newCompatibleTransport(),
                    new AndroidJsonFactory(), null);
        }
    }

    /**
     * Returns a singleton instance of this class
     * @return
     */
    public static NetworkUtilities getInstance() {
        if(mNetworkUtilities == null){
            mNetworkUtilities = new NetworkUtilities();
        }
        return mNetworkUtilities;
    }

    /**
     * Returns the TallBuilding instance
     * @return
     */
    public Tallbuilding getTallBuildingInstance() {
        return mTallBuilding;
    }

    /**
     * Makes a call to take eggs
     * @param listener
     */
    public void takeEggs(IResponseListener listener) {
        new TakeEggsAsyncTask(listener).execute();
    }

    /**
     * Makes a call to drop an egg from the specified floor number
     * @param eggId
     * @param floorNumber
     * @param isRecursive
     * @param traverseFlag
     * @param listener
     */
    public void dropEggs(long eggId, int floorNumber,boolean isRecursive,
                         boolean traverseFlag, IResponseListener listener) {
        new DropEggsAsyncTask(eggId, floorNumber,isRecursive,traverseFlag, listener).execute();
    }

    /**
     * Checks the egg at the particular floor number
     * @param eggId
     * @param floorNumber
     * @param listener
     */
    public void checkEgg(long eggId, int floorNumber, IResponseListener listener) {
        new CheckEggsAsyncTask(eggId, floorNumber, listener).execute();
    }
}
