package com.jamosolutions.dharashah.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;

import com.jamosolutions.dharashah.JamoSolutionsApp;

/**
 * Created by Dhara Shah on 21-01-2016.
 */
public class RestClient {
    /**
     * Checks the availability of internet
     * @return
     */
    public static boolean isNetworkAvailable() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm =
                (ConnectivityManager) JamoSolutionsApp.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = cm.getAllNetworks();
            NetworkInfo networkInfo;
            for (Network network : networks) {
                networkInfo = cm.getNetworkInfo(network);

                if (networkInfo.getTypeName().equalsIgnoreCase("WIFI"))
                    if (networkInfo.isConnected())
                        haveConnectedWifi = true;
                if (networkInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (networkInfo.isConnected())
                        haveConnectedMobile = true;

                if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                    return true;
                }
            }
        }else {
            //noinspection deprecation
            NetworkInfo[] info = cm.getAllNetworkInfo();
            if (info != null) {
                for (NetworkInfo anInfo : info) {
                    if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                        if (anInfo.getTypeName().equalsIgnoreCase("WIFI"))
                            if (anInfo.isConnected())
                                haveConnectedWifi = true;
                        if (anInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                            if (anInfo.isConnected())
                                haveConnectedMobile = true;
                    }
                }
            }
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
}
