package com.jamosolutions.dharashah.callback;

/**
 * Created by Dhara Shah on 24-01-2016.
 */
public interface IResponseListener {
    void onResponseReceived(Object object);
    void onFloorChanged(int floorNumber);
}
